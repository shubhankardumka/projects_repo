# Integers 

print(10)
print(+9)
print(9+9)

# Integers can be combined using operators such as +,-,*,/ . The result of a division always yields a float value 

print(10/9)

# To divide two integers and return an integer, use // . This type of division always floors the calculation

print(10//9)
print(23//6)

# Floats - Literals with a integer and a decimal. Can also be expressed using scientific notation XeY that translates to X * 10 to the power Y 

print(10.3333)
print(10e10) 
